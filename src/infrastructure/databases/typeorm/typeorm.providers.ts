import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserEntity } from '@modules/auth/entity/user.entity';
import { CourseEntity } from '@modules/auth/entity/course.entity';


export const createTypeOrmOptions = (configService: ConfigService): TypeOrmModuleOptions => ({
  type: 'postgres',
  host: configService.get<string>('DATABASE_HOST'),
  port: Number(configService.get<string>('DATABASE_PORT')),
  username: configService.get<string>('DATABASE_USERNAME'),
  password: configService.get<string>('DATABASE_PASSWORD'),
  database: configService.get<string>('DATABASE_NAME'),
  entities: [UserEntity, CourseEntity],
  synchronize: true,
});

export const typeOrmProvider = {
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: createTypeOrmOptions,
  provide: 'DATABASE_CONNECTION',
};
