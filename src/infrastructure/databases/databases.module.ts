import { Module } from '@nestjs/common';
import { RedisModule } from './redis/redis.module';
import { TypeOrmDatabaseModule } from "./typeorm/typeorm.module";

@Module({
  imports: [RedisModule, TypeOrmDatabaseModule],
})
export class DatabasesModule { }
