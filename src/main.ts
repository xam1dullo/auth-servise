import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { join } from 'path';
import * as bodyParser from 'body-parser'; // Ensure body-parser is imported correctly
import { Logger } from '@nestjs/common';
import { AUTH_PACKAGE_NAME } from "./common/types/auth";
import { AuthModule } from "@modules/auth/auth.module";
import { config } from "dotenv";

config()

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.GRPC,
      options: {
        package: AUTH_PACKAGE_NAME,
        protoPath: join(__dirname, '..', '..', 'globals', 'proto', 'auth.proto'),
        url: process.env.GRPC_AUTH_SERVICE
      }
    },
  );


  await app.listen();

  Logger.log(`Application is running on PORT :${process.env.GRPC_AUTH_SERVICE}`)
}

bootstrap();
