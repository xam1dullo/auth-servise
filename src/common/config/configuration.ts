import { config } from "dotenv";
config()


export const configuration = () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  grpc: {
    authService: process.env.GRPC_AUTH_SERVICE,
    coreService: process.env.GRPC_CORE_SERVICE,
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT, 10),
  },
  rabbitmq: {
    url: process.env.RABBITMQ_URL,
  },
  jwtStrategy: process.env.JWT_SECRET
});
