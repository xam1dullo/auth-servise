import { Injectable, Inject, ConflictException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as Redis from 'ioredis';
import * as bcrypt from 'bcrypt';
import {
  AuthServiceController,
  LogoutRequest,
  LogoutResponse,
  SignInRequest,
  SignInResponse,
  SignUpRequest,
  SignUpResponse,
} from '@app/common/types/auth';
import { REDIS } from '@app/common/constants';
import { ConfigService } from '@nestjs/config';
import { UserEntity } from './entity/user.entity';

@Injectable()
export class AuthService implements AuthServiceController {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
    private readonly jwtService: JwtService,
    @Inject(REDIS) private readonly redis: Redis.Redis,
    public readonly configService: ConfigService,
  ) { }

  async signUp(request: SignUpRequest): Promise<SignUpResponse> {
    try {
      const { email } = request;

      const existingUser = await this.usersRepository.findOne({
        where: { email },
      });

      if (existingUser) {
        return {
          message: 'Email already exists',
        };
      }

      const hashedPassword = await bcrypt.hash(request.password, 9);

      const newUser = this.usersRepository.create({
        email,
        password: hashedPassword,
        role: request.role
      });

      const user = await this.usersRepository.save(newUser);

      return { message: 'User successfully registered' };
    } catch (error) {
      throw new Error(error);
    }
  }

  async signIn(request: SignInRequest): Promise<SignInResponse> {
    try {
      const user = await this.usersRepository.findOne({
        where: { email: request.email },
      });

      if (user && (await bcrypt.compare(request.password, user.password))) {


        const payload = { sub: user.id, email: user.email, role: user.role };

        const secret = this.configService.get<string>('JWT_SECRET');
        const expiresIn = this.configService.get<number>('JWT_EXPIRATION');

        const accessToken = this.jwtService.sign(payload, {
          secret,
          expiresIn: expiresIn,
        });


        await this.redis.set(`user_${user.id}`, accessToken, 'EX', expiresIn);

        return {
          token: accessToken,
        }
      } else {
        throw new Error('Invalid credentials');
      }
    } catch (error) {
      throw new Error(error);
    }
  }
  async logout(request: LogoutRequest): Promise<LogoutResponse> {
    const user = await this.jwtService.decode(request.token);

    const foundUser = await this.usersRepository.findOne({
      where: { id: user.sub },
    });

    if (!foundUser) {
      throw new Error('User not found');
    }

    await this.redis.del(`user_${user.id}`);
    return { message: 'User successfully logged out' };
  }

  async getMe(request): Promise<any> {
    try {
      const user = await this.jwtService.decode(request.token);

      const foundUser = await this.usersRepository.findOne({
        where: { id: user.sub },
      });

      if (!foundUser) {
        throw new Error('User not found');
      }
      return {
        id: `${foundUser.id}`,
        email: foundUser.email,
        role: foundUser.role
      };
    } catch (error) {
      throw new Error(error);
    }
  }
}
