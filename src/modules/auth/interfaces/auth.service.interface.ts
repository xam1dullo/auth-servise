import { Observable } from 'rxjs';
import { SignInDto, SignUpDto } from '../dto';

export interface AuthServiceInterface {
  signUp(data: SignUpDto): Observable<any>;
  signIn(data: SignInDto): Observable<any>;
  getMe(data: { token: string }): Observable<any>;
  logout(data: { token: string }): Observable<any>;
}
