import {
  Controller,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Req,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import {
  AuthServiceController,
  AuthServiceControllerMethods,
  GetMeRequest,
  GetMeResponse,
  LogoutRequest,
  LogoutResponse,
  SignInRequest,
  SignInResponse,
  SignUpRequest,
  SignUpResponse,
} from '@app/common/types/auth';

@Controller()
@AuthServiceControllerMethods()
export class AuthController implements AuthServiceController {
  constructor(private readonly authService: AuthService) { }

  // @UsePipes(new ValidationPipe({ whitelist: true }))
  signUp(request: SignUpRequest): Promise<SignUpResponse> {
    return this.authService.signUp(request);
  }

  // @UsePipes(new ValidationPipe({ whitelist: true }))
  signIn(request: SignInRequest): Promise<SignInResponse> {
    console.log({ request })
    return this.authService.signIn(request);
  }

  logout(request: LogoutRequest): Promise<LogoutResponse> {
    return this.authService.logout(request);
  }

  getMe(request: GetMeRequest): Promise<GetMeResponse> {
    return this.authService.getMe(request);
  }
}
