import { Role } from '@app/common';
import { IsEmail, IsString, MinLength, IsEnum } from 'class-validator';


export class SignUpDto {
  @IsEmail()
  email: string;

  @IsString()
  @MinLength(6)
  password: string;

  @IsString()
  @MinLength(6)
  @IsEnum(Role)
  role: Role;
}
